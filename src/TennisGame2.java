
public class TennisGame2 implements TennisGame
{
    private static final int _FORTY = 3;
	private static final int _THIRTY = 2;
	private static final int _FIFTEEN = 1;
	private static final int _LOVE = 0;
	public int player1Points = 0;
    public int player2Points = 0;
    
    private String player1Name;
    private String player2Name;
    
    public TennisGame2(String player1Name, String player2Name) {
    	this.player1Name = player1Name;
    	this.player2Name = player2Name;
    	
    }

    public String getLiteralScore(){
        String literalScore = "";
    	if (isNormal())
    		literalScore = getLiteralForNormal();
    	if(isTie())
    		literalScore = getLiteralForTie();
    	if (isDeuce())
            literalScore = getLiteralForDuece();
        if (iAdvantageOver(player1Points,player2Points))
            literalScore = "Advantage player1";
        if (iAdvantageOver(player2Points,player1Points))
            literalScore = "Advantage player2"; 
        if (isWinner(player1Points,player2Points))
            literalScore = "Win for player1";
        if (isWinner(player2Points,player1Points))
            literalScore = "Win for player2";
        
        return literalScore;
    }

	private String getLiteralForDuece() {
		return "Deuce";
	}

	private String getLiteralForTie() {
		return getLiteral(player1Points) + "-All";
	}

	private String getLiteralForNormal() {
		return getLiteral(player1Points) + "-" + getLiteral(player2Points);
	}

	private boolean isDeuce() {
		return player1Points==player2Points && player1Points>=3;
	}

	private boolean isNormal() {
		return player2Points!=player1Points;
	}

	private boolean isTie() {
		return player1Points == player2Points && player1Points < 4;
	}

	private boolean isWinner(int firstPlayerPoints, int secondPlayerPoints) {
		return firstPlayerPoints>=4 && secondPlayerPoints>=0 && (firstPlayerPoints-secondPlayerPoints)>=2;
	}


	private boolean iAdvantageOver(int firstPlayerPoints,int secondPlayerPoints) {
		return firstPlayerPoints > secondPlayerPoints && secondPlayerPoints >= 3;
	}
	
	private String getLiteral(int playerPoints) {
		String result = "";
		if (playerPoints==_LOVE)
			result = "Love";
		if (playerPoints==_FIFTEEN)
			result = "Fifteen";
		if (playerPoints==_THIRTY)
			result = "Thirty";
		if (playerPoints==_FORTY)
			result = "Forty";
		return result;
	}
    
    public void SetP1Score(int number){
        
        for (int i = 0; i < number; i++)
        {
            P1Score();
        }
            
    }
    
    public void SetP2Score(int number){
        
        for (int i = 0; i < number; i++)
        {
            P2Score();
        }
            
    }
    
    public void P1Score(){
        player1Points++;
    }
    
    public void P2Score(){
        player2Points++;
    }

    public void wonPoint(String player) {
        if (player == "player1")
            P1Score();
        else
            P2Score();
    }
}